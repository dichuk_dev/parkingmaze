package ua.dichuk.parkingmaze.activity;

import android.os.Bundle;

import ua.dichuk.parkingmaze.R;
import ua.dichuk.parkingmaze.activity.core.BaseActivity;
import ua.dichuk.parkingmaze.navigation.ContentManagerUI;
import ua.dichuk.parkingmaze.navigation.manager.ManagerUI;


/**
 * create by project 9.08.2019
 */
public class ContentActivity extends BaseActivity {

    @Override
    protected int getLayoutResource() {
        return R.layout.content_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());
    }

    @Override
    protected ManagerUI getManagerUIToInit() {
        return new ContentManagerUI(this);
    }

}
