package ua.dichuk.parkingmaze.activity.core;

import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import ua.dichuk.parkingmaze.R;
import ua.dichuk.parkingmaze.navigation.core.BaseFragmentData;
import ua.dichuk.parkingmaze.navigation.core.FragmentFeedback;
import ua.dichuk.parkingmaze.navigation.core.ToolbarByID;
import ua.dichuk.parkingmaze.navigation.manager.ManagerUI;
import ua.dichuk.parkingmaze.ui.fragment.DashboardFragment;
import ua.dichuk.parkingmaze.ui.fragment.core.core.BaseFragment;

/**
 * Created by Mykhailo Dichuk on 09/08/19.
 */
public abstract class BaseActivity extends AppCompatActivity implements FragmentFeedback {
    private ManagerUI managerUI;

    private boolean isClickedBackPressed;

    protected abstract ManagerUI getManagerUIToInit();

    @LayoutRes
    protected abstract int getLayoutResource();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        managerUI = getManagerUIToInit();
    }

    @Override
    public void changeFragmentTo(BaseFragmentData baseFragmentData) {
        this.managerUI.changeFragmentTo(baseFragmentData);
    }

    @Override
    public void initToolBar(BaseFragment baseFragment, ToolbarByID toolBarById, int... label) {
        this.managerUI.initToolBar(baseFragment, toolBarById, label);
    }

    @Override
    public void onBackPressed() {
        if (!this.isClickedBackPressed && isVisibleFragment(DashboardFragment.class.getSimpleName())) {
            this.doubleBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @StringRes
    public int getExitWithDoubleClickText() {
        return R.string.click_again_to_exit;
    }

    private boolean isVisibleFragment(String tag) {
        Fragment fragment = this.getSupportFragmentManager().findFragmentByTag(tag);
        return fragment != null && fragment.isVisible();
    }

    private void doubleBackPressed() {
        if (!this.isClickedBackPressed) {
//            Toast.makeText(this.getSupportFragmentManager().findFragmentByTag(DashboardFragment.class.getSimpleName()).getView(), "" + this.getExitWithDoubleClickText(), Toast.LENGTH_LONG).show();
            this.isClickedBackPressed = true;
            (new Handler()).postDelayed(() -> this.isClickedBackPressed = false, 2000L);
        }
    }
}
