package ua.dichuk.parkingmaze.navigation.manager;

import androidx.annotation.StringRes;

import ua.dichuk.parkingmaze.navigation.core.BaseFragmentData;
import ua.dichuk.parkingmaze.navigation.core.ToolbarByID;
import ua.dichuk.parkingmaze.ui.fragment.core.core.BaseFragment;

/**
 * Created by Mykhailo Dichuk on 09/08/19.
 */
public interface ManagerUI {

    void changeFragmentTo(BaseFragmentData baseFragmentData);

    void initToolBar(BaseFragment baseFragment, ToolbarByID toolBarById, @StringRes int... label);
}
