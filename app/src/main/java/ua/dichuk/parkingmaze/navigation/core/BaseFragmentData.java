package ua.dichuk.parkingmaze.navigation.core;

import android.os.Bundle;

/**
 * Created by Mykhailo Dichuk on 09/08/19.
 */
public abstract class BaseFragmentData {
    protected Bundle bundle;

    public BaseFragmentData setBundle(Bundle bundle) {
        this.bundle = bundle;
        return this;
    }

    public Bundle getBundle() {
        return bundle;
    }

    public abstract FragmentByID getFragmentById();
}
