package ua.dichuk.parkingmaze.navigation;

import androidx.appcompat.app.AppCompatActivity;

import ua.dichuk.parkingmaze.R;
import ua.dichuk.parkingmaze.navigation.core.BaseFragmentData;
import ua.dichuk.parkingmaze.navigation.core.FragmentByID;
import ua.dichuk.parkingmaze.navigation.core.FragmentData;
import ua.dichuk.parkingmaze.navigation.core.ToolbarByID;
import ua.dichuk.parkingmaze.navigation.manager.BaseManagerUI;
import ua.dichuk.parkingmaze.ui.fragment.DashboardFragment;
import ua.dichuk.parkingmaze.ui.fragment.SplashFragment;
import ua.dichuk.parkingmaze.ui.fragment.core.core.BaseFragment;

/**
 * Created by Mykhailo Dichuk on 09/08/19.
 */
public class ContentManagerUI extends BaseManagerUI {

    public ContentManagerUI(AppCompatActivity activity) {
        super(activity);
    }

    @Override
    protected int getIdFragmentsContainer() {
        return R.id.fragment_container;
    }

    @Override
    protected void initUI() {
        changeFragmentTo(new FragmentData(FragmentByID.SPLASH_FRAGMENT));

    }

    @Override
    protected Class<?> getFirstInStackFragmentClass() {
        return DashboardFragment.class;
    }

    @Override
    public void changeFragmentTo(BaseFragmentData fragmentData) {
        FragmentByID id = fragmentData.getFragmentById();
        switch (id) {
            case SPLASH_FRAGMENT: {
                addFragmentToContainer(new SplashFragment(), false, fragmentData.getBundle());
                break;
            }
            case DASHBOARD_FRAGMENT: {
                addFragmentToContainer(new DashboardFragment(), false, fragmentData.getBundle());
                break;
            }
        }
    }

    @Override
    public void initToolBar(BaseFragment baseFragment, ToolbarByID toolBarById, int... label) {

    }
}
