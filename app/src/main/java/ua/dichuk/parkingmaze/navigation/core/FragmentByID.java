package ua.dichuk.parkingmaze.navigation.core;

/**
 * Created by Mykhailo Dichuk on 09/08/19.
 */
public enum FragmentByID {
    SPLASH_FRAGMENT,
    DASHBOARD_FRAGMENT,
}
