package ua.dichuk.parkingmaze.navigation.manager;

import android.os.Bundle;

import androidx.annotation.AnimRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import ua.dichuk.parkingmaze.R;
import ua.dichuk.parkingmaze.ui.fragment.core.core.BaseFragment;
import ua.dichuk.parkingmaze.utils.FragmentAnimUtils;

/**
 * Created by Mykhailo Dichuk on 09/08/19.
 */
public abstract class BaseManagerUI implements ManagerUI {
    private AppCompatActivity activity;
    private boolean useFragmentAnim = true;

    public BaseManagerUI(AppCompatActivity activity) {
        this.activity = activity;
        this.initUI();
    }

    public AppCompatActivity getActivity() {
        return activity;
    }

    protected abstract int getIdFragmentsContainer();

    protected abstract void initUI();

    protected void addFragmentToContainer(BaseFragment fragment, boolean toBackStack, Bundle bundle) {
        fragment.setArguments(bundle);
        FragmentManager fm = this.getActivity().getSupportFragmentManager();
        BaseFragment dashboardFragment;
        if (fragment.getClass() == this.getFirstInStackFragmentClass()) {
            if (this.useFragmentAnim) {
                FragmentAnimUtils.revertAnim();
            }

            dashboardFragment = (BaseFragment) this.getActivity().getSupportFragmentManager().findFragmentByTag(this.getFirstInStackFragmentClass().getSimpleName());
            if (dashboardFragment != null) {
                for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                    fm.popBackStack();
                }
                return;
            }
        }
        dashboardFragment = (BaseFragment) fm.findFragmentByTag(fragment.getClass().getSimpleName());
        if (dashboardFragment != null && dashboardFragment.isAdded()) {

        } else {
            FragmentTransaction transaction = fm.beginTransaction();
            if (this.useFragmentAnim) {
                transaction.setCustomAnimations(this.getFragmentAnimationEnter(), this.getFragmentAnimationExit(), this.getFragmentAnimationPopEnter(), this.getFragmentAnimationPopExit());
            }

            if (toBackStack) {
                transaction.addToBackStack(fragment.getClass().getSimpleName()).replace(this.getIdFragmentsContainer(), fragment, fragment.getClass().getSimpleName());
            } else {
                transaction.replace(this.getIdFragmentsContainer(), fragment, fragment.getClass().getSimpleName());
            }
            transaction.commitAllowingStateLoss();
        }
    }

    protected abstract Class<?> getFirstInStackFragmentClass();

    @AnimRes
    protected int getFragmentAnimationEnter() {
        return R.anim.fragment_animation_enter;
    }

    @AnimRes
    protected int getFragmentAnimationExit() {
        return R.anim.fragment_animation_exit;
    }

    @AnimRes
    protected int getFragmentAnimationPopEnter() {
        return R.anim.fragment_animation_pop_enter;
    }

    @AnimRes
    protected int getFragmentAnimationPopExit() {
        return R.anim.fragment_animation_pop_exit;

    }
}
