package ua.dichuk.parkingmaze.navigation.core;

import androidx.annotation.StringRes;

import ua.dichuk.parkingmaze.ui.fragment.core.core.BaseFragment;

/**
 * Created by Mykhailo Dichuk on 09/08/19.
 */
public interface FragmentFeedback {
    void changeFragmentTo(BaseFragmentData baseFragmentData);

    void initToolBar(BaseFragment baseFragment, ToolbarByID toolBarById, @StringRes int... label);

    void onBackPressed();
}