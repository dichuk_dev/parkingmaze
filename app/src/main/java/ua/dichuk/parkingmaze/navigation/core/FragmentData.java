package ua.dichuk.parkingmaze.navigation.core;

import android.os.Bundle;

/**
 * Created by Mykhailo Dichuk on 09/08/19.
 */
public class FragmentData extends BaseFragmentData {

    private FragmentByID fragmentByID;

    public FragmentData(FragmentByID fragmentById) {
        this.fragmentByID = fragmentById;
    }


    public FragmentData(FragmentByID fragmentById, Bundle bundle) {
        this.fragmentByID = fragmentById;
        this.bundle = bundle;
    }

    @Override
    public FragmentData setBundle(Bundle bundle) {
        return (FragmentData) super.setBundle(bundle);
    }

    @Override
    public Bundle getBundle() {
        if (bundle != null)
            return bundle;
        return new Bundle();
    }

    @Override
    public FragmentByID getFragmentById() {
        return fragmentByID;
    }
}
