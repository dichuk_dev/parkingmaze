package ua.dichuk.parkingmaze.ui.fragment.core.core;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;

import ua.dichuk.parkingmaze.R;
import ua.dichuk.parkingmaze.activity.ContentActivity;
import ua.dichuk.parkingmaze.navigation.core.BaseFragmentData;
import ua.dichuk.parkingmaze.navigation.core.FragmentFeedback;

/**
 * Created by Mykhailo Dichuk on 09/08/19.
 */
public abstract class BaseFragment extends Fragment implements View.OnClickListener {

    private String TAG;
    private Bundle outStateData;
    protected LinearLayout toolbarContainer;
    protected View root;
    private Context context;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            ViewDataBinding vb = DataBindingUtil.inflate(inflater, this.getLayoutResource(), null, false);
            root = vb.getRoot();
            this.toolbarContainer = root.findViewById(R.id.toolbar_container);
        } catch (NullPointerException | NoClassDefFoundError var6) {
            root = inflater.inflate(this.getLayoutResource(), null);
            this.toolbarContainer = root.findViewById(R.id.toolbar_container);
        }
        this.initUI(root);
        this.setListeners();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.onFragmentRegisteredAsObserver();
    }

    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putBundle("outStateData", this.outStateData);
        super.onSaveInstanceState(outState);
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            this.outStateData = savedInstanceState.getBundle("outStateData");
        }
        this.TAG = this.getClass().getSimpleName() + String.valueOf(this.hashCode());
        this.setRetainInstance(true);
    }

    public void setArguments(Bundle args) {
        this.outStateData = args;
        super.setArguments(args);
    }


    @LayoutRes
    protected abstract int getLayoutResource();

    protected void initUI(View view) {
    }

    protected void setListeners() {
    }

    @Override
    public void onClick(View v) {

    }

    public LinearLayout getToolbarContainer() {
        return toolbarContainer;
    }

    public String getCustomTAG() {
        return this.TAG;
    }

    protected void onFragmentRegisteredAsObserver() {

    }

    protected FragmentFeedback getFragmentFeedback() {
        return this.getContentActivity();
    }

    public void changeFragmentTo(BaseFragmentData baseFragmentData) {
        if (this.getFragmentFeedback() != null) {
            this.getFragmentFeedback().changeFragmentTo(baseFragmentData);
        }
    }

    @Nullable
    protected Bundle getFragmentData() {
        return this.outStateData;
    }

    protected ContentActivity getContentActivity() {
        if (getActivity() == null)
            return (ContentActivity) context;
        return (ContentActivity) getActivity();
    }

    public void onBackPressed() {
        if (this.getFragmentFeedback() != null) {
            this.getFragmentFeedback().onBackPressed();
        }
    }
}
