package ua.dichuk.parkingmaze.ui.fragment;


import android.os.Handler;

import ua.dichuk.parkingmaze.R;
import ua.dichuk.parkingmaze.navigation.core.FragmentByID;
import ua.dichuk.parkingmaze.navigation.core.FragmentData;
import ua.dichuk.parkingmaze.ui.fragment.core.AppFragment;

/**
 * Created by Mykhailo Dichuk on 09/08/19.
 */
public class SplashFragment extends AppFragment {

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_splash;
    }

    @Override
    protected void onFragmentRegisteredAsObserver() {
        new Handler().postDelayed(this::goNext, 3000);
    }

    private void goNext() {
        changeFragmentTo(new FragmentData(FragmentByID.DASHBOARD_FRAGMENT));
    }
}
