package ua.dichuk.parkingmaze.ui.fragment.core;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

import ua.dichuk.parkingmaze.navigation.core.ToolbarByID;
import ua.dichuk.parkingmaze.ui.fragment.core.core.BaseFragment;

/**
 * Created by Mykhailo Dichuk on 09/08/19.
 */
public abstract class AppFragment extends BaseFragment {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        resumeBundle();
    }

    protected void resumeBundle() {

    }

    protected void initToolBar(ToolbarByID toolBarById, @StringRes int label) {
        if (this.getFragmentFeedback() != null) {
            this.getFragmentFeedback().initToolBar(this, toolBarById, label);
        }
    }
}
