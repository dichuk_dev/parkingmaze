package ua.dichuk.parkingmaze.ui.fragment;

import ua.dichuk.parkingmaze.R;
import ua.dichuk.parkingmaze.ui.fragment.core.AppFragment;

/**
 * Created by Mykhailo Dichuk on 09/08/19.
 */
public class DashboardFragment extends AppFragment {

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_dashboard;
    }

}
