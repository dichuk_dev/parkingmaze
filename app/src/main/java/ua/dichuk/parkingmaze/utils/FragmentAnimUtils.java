package ua.dichuk.parkingmaze.utils;

/**
 * Created by Mykhailo Dichuk on 09/08/19.
 */
public class FragmentAnimUtils {

    private static boolean isRevertAnim;

    public FragmentAnimUtils() {

    }

    public static void revertAnim() {
        isRevertAnim = true;
    }

    public static void restoreAnim() {
        isRevertAnim = false;
    }

    public static boolean isRevertAnim() {
        return isRevertAnim;
    }
}
